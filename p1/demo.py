# BE SURE the necessary packages are installed! pip = "Pip Installs Packages"
# pip freeze

# Starting in 0.19.0, pandas no longer supports pandas.io.data or pandas.io.wb
# Must replace iomports from pandas.io with those from pandas_datareader:
# https://pandas-datareader.readthedocs.io/en/latest/remote_data.html
import pandas as pd # Pandas = "Python Data Analysis Library"
import datetime
import pandas_datareader as pdr # remote data access for pandas
import matplotlib.pyplot as pyplot
from matplotlib import style

start = datetime.datetime(2010, 1, 1)
end = datetime.datetime.now()

# NOTE: XOM is stock market sumbol for Exxon Mobil Corporation
df = pdr.DataReader("XOM", "yahoo", start, end)

print("\nPrint number of records: ")
# statement goes here

# Why is it important to run the following print statement...
print(df.columns)

print("\nPrint data frame: ")
print(df) # Note: for efficiency, only prints 60--not *all* records

print("\nPrint first five lines:")
# Note: "Date" is lower than the other columns as it is treated as as index
# statement goes here...

print("\nPrint last five lines:")
# statement goes here...

print("\nPrint first 2 lines:")
# statement goes here...

print("\nPrint last 2 lines:")
# statement goes here...

# Research what these styles do!
# style.use('fivethirtyeight')
# compare with...
style.use('ggplot')

df['High'].plot()
df['Adj Close'].plot()
plt.legend()
plt.show()