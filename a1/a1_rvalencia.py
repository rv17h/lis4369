subtotal = 0.00
taxPercent = 0.0
tipPercent = 0.0
total = 0.00
eachPay = 0.00
split = 0

# Display title
print("\nTip Calculator\n")

# Requirements
print("Program Requirements:")
print("1. Must use float data type for user input (except, \"Party Number\").")
print("2. Must round calculations to two decimal places.")
print("3. Must format currency with dollar sign, and two decimal places.")

# User Input
print("\nUser Input:")
subtotal = float(input("Cost of meal: "))
taxPercent = float(input("Tax percent: "))
tipPercent = float(input("Tip percent: "))
split = int(input("Party number: "))

# Calculate tax, amount due, gratuity, total, and how much each user pays
tax = (subtotal * taxPercent) / 100
amountDue = subtotal + tax
gratuity = (amountDue * tipPercent) / 100
total = subtotal + tax + gratuity
eachPay = total / split

# Display the results
print("\nProgram Output:")
print("Subtotal:\t" + '${:,.2f}'.format(subtotal))
print("Tax:\t\t" + '${:,.2f}'.format(tax))
print("Amount Due:\t" + '${:,.2f}'.format(amountDue))
print("Gratuity:\t" + '${:,.2f}'.format(gratuity))
print("Total:\t\t" + '${:,.2f}'.format(total))
print("Split (" + str(split) + "):\t" + '${:,.2f}'.format(eachPay))