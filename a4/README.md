# LIS4369 - Extensible Enterprise Solutions

## Rene Valencia

### Assignment 4 Requirements:

_Four Parts:_

1. Distributed Version Control with Git and Bitbucket
2. Development Installations
3. Questions
4. Bitbucket repo links:  

    a) https://bitbucket.org/rv17h/lis4369/src/master/  
    
    b) https://bitbucket.org/rv17h/lis4369/src/master/a4/

README.md file should include the following items:
    * Screenshot of a4_data_analysis application running  
    * git commands w/ short descriptions
    
> "This is a blockquote.
> 
> This is the second paragraph in the block quote.
> 
> Git commands w/ short descriptions:"

    1. git init - creates a new git repository
    2. git status - get the status of local repository or file
    3. git add - tells git to track new files
    4. git commit - takes staging changes and commits them to project history
    5. git push - pushes commited changes to repository
    6. git pull - pulls changes from remote repository into local repository
    7. git clone - clones a repository
    
Assignment Screenshots:

Data Analysis:

![Visual Studio Code](https://bitbucket.org/rv17h/lis4369/raw/402a82228821e9059079d822d13cc680e85956e0/a4/1.png)
![Visual Studio Code](https://bitbucket.org/rv17h/lis4369/raw/402a82228821e9059079d822d13cc680e85956e0/a4/2.png)