BASE_HOURS = 40     # base hours
OT_RATE = 1.5     # overtime rate
HOLIDAY_RATE = 2.0  # holiday rate 

def main():
  get_requirements()
  calculate_payroll(0, 0, 0, 0, 0, 0, 0, 0)

def get_requirements():
  print("Payroll Calculator")
  print("\nProgram Requirements:\n"
    + "1. Must use float data type for user input.\n"
    + "2. Overtime rate: 1.5 times hourly rate (hours over 40).\n"
    + "3. Holiday rate: 2.0 times hourly rate (all holiday hours).\n"
    + "4. Must format currency with dollar sign, and round to two decimal places.\n"
    + "5. Create at least three functions that are called by the program:\n"
    + "\ta. main(): calls at least two other functions.\n"
    + "\tb. get_requirements(): displays the program requirements.\n"
    + "\tc. calculate_payroll(): calculates an individual one-week paycheck."
  )

def calculate_payroll(hours, holiday_hours, pay_rate, base_pay, overtime_hours, overtime_pay, holiday_pay, gross_pay):
  print("\nInput:")
  # get hours worked and hourly pay rate
  hours = float(input('Enter hours worked: '))
  holiday_hours = float(input('Enter holiday hours: '))
  pay_rate = float(input('Enter hourly pay rate: '))
  base_pay = BASE_HOURS * pay_rate
  overtime_hours = hours - BASE_HOURS
  overtime_pay = (OT_RATE * pay_rate) * overtime_hours
  holiday_pay = (HOLIDAY_RATE * pay_rate) * holiday_hours
  gross_pay = base_pay + overtime_pay + holiday_pay

  print("\nOutput:")
  print("Base:      " + '${:,.2f}'.format(base_pay))
  print("Overtime:  " + '${:,.2f}'.format(overtime_pay))
  print("Holiday:   " + '${:,.2f}'.format(holiday_pay))
  print("Gross:     " + '${:,.2f}'.format(gross_pay))
  return hours, holiday_hours, pay_rate, base_pay, overtime_hours, overtime_pay, holiday_pay, gross_pay
  # return base_pay, overtime_hours, overtime_pay, holiday_pay, gross_pay
  

if __name__ == "__main__":
  main()  