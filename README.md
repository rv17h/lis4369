# LIS4369 - Extensible Enterprise Solutions

## Rene Valencia

### LIS4369 Requirements:

_Course Work Links:_

1. [A1 README.md](https://bitbucket.org/rv17h/lis4369/src/master/a1/README.md)
	* Install Python
	* Install R
	* Install R Studio
	* Install Visual Studio Code
	* Create a1_tip_calculator application
	* Provide screenshots of installations
	* Create Bitbucket repo
	* Complete Bitbucket tutorial (bitbucketstationlocations)
	* Provide git command descriptions
2. [A2 README.md](https://bitbucket.org/rv17h/lis4369/src/master/a2/README.md)
	* Create a2_payroll application
	* Provide screenshots of installations
3. [A3 README.md](https://bitbucket.org/rv17h/lis4369/src/master/a3/README.md)
	* Create a3_painting_estimator application
	* Screenshot of a3 application running
4. [A4 README.md](https://bitbucket.org/rv17h/lis4369/src/master/a4/README.md)
	* Create a4_data_analysis
	* Screenshot of a4 application running
5. [A5 README.md](https://bitbucket.org/rv17h/lis4369/src/master/a5/README.md)
	* Create a5.R  
	* Screenshot of a5 application running  
	* Two plots
6. [P1 README.md](https://bitbucket.org/rv17h/lis4369/src/master/p1/README.md)
	* Create p1 application
7. [P2 README.md](https://bitbucket.org/rv17h/lis4369/src/master/p2/README.md)
	* Create p2.R
	* Two plots