# LIS4369 - Extensible Enterprise Solutions

## Rene Valencia

### Assignment 5 Requirements:

_Four Parts:_

1. Distributed Version Control with Git and Bitbucket
2. Development Installations
3. Questions
4. Bitbucket repo links:  

    a) https://bitbucket.org/rv17h/lis4369/src/master/  
    
    b) https://bitbucket.org/rv17h/lis4369/src/master/a5/

README.md file should include the following items:
    * Screenshot of a5 application running  
    * Two plots
    
> "This is a blockquote.
> 
> This is the second paragraph in the block quote.
> 
> Git commands w/ short descriptions:"

    1. git init - creates a new git repository
    2. git status - get the status of local repository or file
    3. git add - tells git to track new files
    4. git commit - takes staging changes and commits them to project history
    5. git push - pushes commited changes to repository
    6. git pull - pulls changes from remote repository into local repository
    7. git clone - clones a repository
    
Assignment Screenshots:

Data Analysis:

![Visual Studio Code](https://bitbucket.org/rv17h/lis4369/raw/ceba3404b121c7034c5f5227d981d0233915c675/a5/a5_1.png)
![Visual Studio Code](https://bitbucket.org/rv17h/lis4369/raw/ceba3404b121c7034c5f5227d981d0233915c675/a5/a5_2.png)
![Visual Studio Code](https://bitbucket.org/rv17h/lis4369/raw/87542d466dad7b84835afc7eaf08a9e45e5c2a38/a5/a5_3.png)